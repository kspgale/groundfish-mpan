#Want to make the map code super generalizable

#Maps of top deciles
library(sp)
library(rgdal)
library(maptools)
library(raster)
library(gridExtra)

# setwd("E:/Documents/Data files/MPATT Data/Groundfish/")
setwd("E:/Documents/Data files/MPATT Data/Groundfish/RescaleCombineThenKDE/MPATTFilesClip/")


## Enter Parameters

surveyName<-c("normCombine") #
nbhd<-10 #neighbourhood, in km
analysis<-c("")

####

#folder1<-c(paste0("./",analysis,"/",nbhd, "kmNeighbour_line/2.ReclassifyPercentile/"))
folder1<-c(paste0("./","R_Reclassify_2018.03.09/"))
# survey1<-tolower(surveyName)
survey1<-(surveyName)
#mapFolder<-c(paste0("./",analysis,"/",nbhd, "kmNeighbour_line/Maps/"))
mapFolder<-c(paste0("./",analysis,"/Maps/"))


dataPointLoc<-c(paste0("./","Input/"))
listDataPoint<-data.frame(files=list.files(dataPointLoc, pattern="*.shp$"))
listDataPoint$Code<-gsub("_normalize","",listDataPoint$files)
listDataPoint$Code<-gsub("_syn-sab-phma","",listDataPoint$Code)
listDataPoint$Code<-gsub("_syn-phma","",listDataPoint$Code)
listDataPoint$Code<-gsub("_syn","",listDataPoint$Code)
listDataPoint$Code<-gsub("_phma","",listDataPoint$Code)
listDataPoint$Code<-gsub(".shp","",listDataPoint$Code)

# # dataPoints<-readOGR("E:/Documents/Data files/MPATT Data/Groundfish/Synoptic_PU/Input/SynopticFELinesSp.shp")
# dataPoints<-readOGR("E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/Outputs/Shapefiles/Synoptics/diversityRichness/GFSynopticSubsample_DiversityFISH_WEIGHT_nSp_Alb.shp")
# # dataPoints<-readOGR("E:/Documents/Projects/EBSAs/Analyses/Shelf/GFData_Prep/2017.02.27 Subsample/Outputs/Shapefiles/PHMA/diversityRichness/GFPHMA_Subsample_DiversityFISH_CountHrHook_nSp_Alb.shp")


# -------------------------------------------------#
# load rasters and set up species names
KDfiles<-list.files(folder1, ("*.tif$"))
KDspecies<-gsub(paste0("kernel_",survey1,"_", nbhd,"km_"),"",KDfiles)
KDspecies<-as.data.frame(gsub("_R_reclassify.tif","",KDspecies))
KDspecies$file<-KDfiles
names(KDspecies)<-c("shortName", "file")

#Combine lists of species
SynNames<-read.csv("E:/Documents/Data files/Data Extractions and Sharing/Groundfish/5.Shapefiles/Synoptic_2017-10-13_FExTaxon_species_names.csv")
SabNames<-read.csv("E:/Documents/Data files/Data Extractions and Sharing/Groundfish/5.Shapefiles/SablefishRandom_2017-08-04_FExTaxon_species_names.csv")
PHMANames<-read.csv("E:/Documents/Data files/Data Extractions and Sharing/Groundfish/5.Shapefiles/PHMA_2017-04-18_fish_species_siteXtaxon_Lines_names.csv")

names<-rbind.data.frame(SynNames, SabNames, PHMANames)
names$origName<-gsub("\\."," ",names$origName)
names$shortName<-gsub("\\.","\\_",names$shortName)
names<-unique(names)
nrow(names) #458 total

#Bring in conservatin priority list
cps<-read.delim("E:/Documents/Projects/MPAT/3.Conservation Priorities/3.Screen/CPList2.txt", sep="\t")
nrow(cps) #84 CPs
cps$ValidName<-as.character(cps$ValidName)
cps$ValidName[cps$ValidName=="Sebastes aleutianus"]<-"Sebastes aleutianus melanostictus"
cps$ValidName[cps$CommonName=="Pacific Sleeper Shark"]<-"Somniosus pacificus"
cps$ValidName[cps$CommonName=="Pacific Herring"]<-"Clupea pallasii pallasii"

names$origName[names$origName=="Ammodytes personatus"]<-"Ammodytes hexapterus"
names$origName[names$origName=="Doryteuthis  Amerigo  opalescens"]<-"Doryteuthis opalescens"

names<-names[names$origName %in% cps$ValidName,]
nrow(names) #65 CPs in these 3 surveys 
cps[!cps$ValidName %in% names$origName,]
names(cps)[1]<-c("origName")
names<-merge(names, cps, by="origName", all.x=T, all.y=F)

source<-read.csv("E:/Documents/Data files/MPATT Data/Groundfish/RescaleCombineThenKDE/SpeciesDatasources.csv")
names<-merge(names, source, by="CommonName", all.x=F, all.y=T)
names<-merge(KDspecies, names, by="shortName", all.x=F, all.y=F)


# # # -------------------------------------------------#
# # # load polygons
shoreline <- readOGR("E:/Documents/!GIS Files/Coastlines/DFO_BC_COASTLIN_AND_BC_BDY_MERGE.shp")

nsb <- readOGR("E:/Documents/!GIS Files/Management_Classification_Areas/NSB.shp")
proj4string(nsb) <- CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs")

contours <- readOGR("E:/Documents/!GIS Files/Bathymetry/bc_eez_100m/Derivatives/contours25m.shp")
proj4string(contours) <- CRS("+proj=aea +lat_1=50 +lat_2=58.5 +lat_0=45 +lon_0=-126 +x_0=1000000 +y_0=0 +datum=NAD83 +units=m +no_defs")

# -------------------------------------------------#
# #Map parameters
# #phma/synoptic
ylim <- c(555000, 1100000)
xlim <- c(640000,760000  )
#full NSB
# ylim <- c(490000, 1105000)
# xlim <- c(470000,1090000  )

#axis labels
# y axis:
x <- -135 # x value in longitude at the y axis
ylab <- seq(-90,90,2) # you can adapt this sequence if you want more or less labels
yS <- SpatialPoints(cbind(x,ylab), proj4string = CRS("+proj=longlat +datum=NAD83"))
ySP<- spTransform(yS, proj4string(shoreline)) 

# x axis
y <- 50 # y value in latitude at the x axis
xlab = seq(-180,180,2)
xS <- SpatialPoints(cbind(xlab,y), proj4string = CRS("+proj=longlat +datum=NAD83"))
xSP<- spTransform(xS, proj4string(shoreline))

cola <- rgb(0,.6,.8, alpha=0.2)
colb <- rgb(0,.6,.8, alpha=0.2)
col1 <- rgb((244/255), (59/255), (32/255), alpha=0.7) #red
col2 <- rgb((217/255), (139/255), (23/255), alpha=0.7) #gold
col3 <- rgb((33/255), (161/255), (76/255), alpha=0.7) #green


# ##---###
# # Loop plots - TOP 10%/BOTTOM 90% for Given Folder
# ##---###
# 
# for (i in 1:nrow(names)){
# 
#   ras<-raster(paste0(folder1, names[i,2]))
#   rastop<-ras
#   rasbottom<-ras
# 
#   rastop[rastop!=10]<-NA
#   rasbottom[!(rasbottom %in% c(1,2,3,4,5,6,7,8,9))]<-NA
#   ras[!(ras %in% c(1,2,3,4,5,6,7,8,9,10))]<-NA
# 
#   # #optional - top 20%
#   # rastop[!rastop%in%c(9,10)]<-NA
#   # rasbottom[!(rasbottom %in% c(1,2,3,4,5,6,7,8))]<-NA
#   # ras[!(ras %in% c(1,2,3,4,5,6,7,8))]<-NA
# 
#     png(paste0(mapFolder,"Hotspots/hotspot_", gsub("\\n","",names$species[i]),"_", survey1, "4.png"), height=15, width=12, units="cm", res=400)
#     par(mar=c(1,1,1,1))
#     plot(shoreline, col=NA, border=NA,xlim=xlim, ylim=ylim)
#     plot(contours[contours@data$CONTOUR==-500,], add=T)
#     plot(contours[contours@data$CONTOUR==-200,], add=T, lwd=0.7)
#     plot(contours[contours@data$CONTOUR==-50,], add=T, lty=2, lwd=0.7)
#     plot(rasbottom, col=colb, add=T, legend=F)
#     plot(rastop, col=col2, add=T, legend=F)
#     plot(nsb, add=T, col=NA, border="gray", lwd=0.8)
#     plot(shoreline, col="gray", border=NA, add=T)
#     rng <- par("usr") # Grab the plotting region dimensions
#     box( lty = 'solid', col = 'black')
#     axis(side = 1, pos=rng[3], at = xSP@coords[,'xlab'], lab=xlab,  tcl = .2,padj=-2.9, cex.axis=.6)
#     axis(side = 2, pos=rng[1], at = ySP@coords[,'ylab'], lab=ylab, las=1, tcl = .2,hadj=-.5, cex.axis=.6)
#     legend("bottomleft", legend=c("Top decile", "Kernel density extent"
#                                   # ,"500 m", "200 m", "50 m", "NSB"
#                                   ),
#            title=paste0(names[i,1]), pch=c(15,15
#                                            # ,NA,NA,NA,NA
#                                            ), lty=c(0,0
#                                                     # ,6,1,2,1
#                                                     ), lwd=c(0, 0
#                                                              # , 1.2,0.7, 0.7, 0.8
#                                                              ),col=c(col2, cola
#                                                                      # ,"black","black","black","gray"
#                                                                      ), cex=0.7, bty="n")
#     # text(850000,ylim[2]-10000,  paste(names$CommonName[i]))
#     legend("bottom", legend=c("500 m", "200 m", "50 m", "NSB"),lty=c(6,1,2,1),lwd=c(1.2,0.7, 0.7, 0.8),col=c("black","black","black","gray"),cex=0.7, bty="n")
#     
#     dev.off()
# }

# 
# ##---###
# # Loop plots -- PERCENTILES (rainbow)
# ##---###
# {colfunc <- colorRampPalette(c("royalblue","springgreen","yellow","red"))
#  colors<-list()
#  for (i in c(1:10)){
#    colors[i]<-paste(colfunc(10)[i], "CC",sep="")}
#  colors<-unlist(colors)
# 
#  for (i in 1:nrow(names)){
#    for (i in c(6,63:67)){   
#    ras<-raster(paste0(folder1, names[i,2]))
#    rastop<-ras
#    rasbottom<-ras
#    rastop[rastop!=10]<-NA
#    rasbottom[!(rasbottom %in% c(1,2,3,4,5,6,7,8,9))]<-NA
#    ras[!(ras %in% c(1,2,3,4,5,6,7,8,9,10))]<-NA
# 
# png(paste0(mapFolder,"Percentiles/percentile_", names$species[i],"_", survey1, ".png"), height=15, width=12, units="cm", res=400)
#  par(mar=c(1,1,1,1))
#  plot(shoreline, col=NA, border=NA,xlim=xlim, ylim=ylim)
#  plot(contours[contours@data$CONTOUR==-500,], add=T, lwd=0.8,col="black")
#  plot(contours[contours@data$CONTOUR==-200,], add=T, lwd=0.7, col="black")
#  plot(contours[contours@data$CONTOUR==-50,], add=T, lwd=0.4, col="black")
#  
#  plot(nsb, add=T, col=NA, lwd=0.7, lty=5, lwd=1.5)
#  plot(shoreline, col="gray", border="gray", add=T)
# plot(ras, add=T, legend=F, col=c(paste(colors)))
# #  plot(synPt, pch=16, cex=0.2, add=T,col=rgb(0,0,1,alpha=0.5))
#  rng <- par("usr") # Grab the plotting region dimensions
#  box( lty = 'solid', col = 'black')
#  axis(side = 1, pos=rng[3], at = xSP@coords[,'xlab'], lab=xlab,  tcl = .2,padj=-2.9, cex.axis=.6)
#  axis(side = 2, pos=rng[1], at = ySP@coords[,'ylab'], lab=ylab, las=1, tcl = .2,hadj=-.5, cex.axis=.6)
# #  legend("bottomleft", legend=c("1-10th","11-20th","21-30th","31-40th","41-50th","51-60th","61-70th","71-80th","81-90th","91-100th","Sampling location"),
# #         title=c("Percentile"), pch=c(15,15,15,15,15,15,15,15,15,15,1,16),    col=c(colfunc(10),"blue"), cex=0.7       , pt.cex=c( rep(1,10),0.4, 0.2))
# legend("bottomleft", legend=c("1-10th","11-20th","21-30th","31-40th","41-50th","51-60th","61-70th","71-80th","81-90th","91-100th"),
#        title=c("Percentile"), pch=c(15,15,15,15,15,15,15,15,15,15),    col=c(colfunc(10)), cex=0.7       , pt.cex=c( rep(1,10),0.4))
# legend("bottom", legend=c("500 m", "200 m", "50 m", "NSB"),lty=c(1,1,1,5),lwd=c(.8,0.7, 0.4, 1.5),col=c("black","black","black","black"),cex=0.7,
# #         title=paste0(surveyName, ", ",nbhd, " km"))
#           title="Synoptic Trawl Survey")
# text(900000,ylim[2]+56000,   paste(names$CommonName[i], "\nSynoptic Trawl Survey"))
#  dev.off()}
# }

##---###
# Loop plots -- PERCENTILES (rainbow) with sample points
##---###
# colfunc <- colorRampPalette(c("royalblue","springgreen","yellow","red"))
colfunc <- colorRampPalette(c("royalblue4","mediumseagreen","khaki","red3"))
 colors<-list()
 for (i in c(1:10)){
   colors[i]<-paste(colfunc(10)[i], "CC",sep="")}
 colors<-unlist(colors)
 
 for (i in 1:nrow(names)){
 for (i in c(3,21)){
   ras<-raster(paste0(folder1, names[i,2]))
   rastop<-ras
   rasbottom<-ras
   rastop[rastop!=10]<-NA
   rasbottom[!(rasbottom %in% c(1,2,3,4,5,6,7,8,9))]<-NA  
   ras[!(ras %in% c(1,2,3,4,5,6,7,8,9,10))]<-NA
   
   spPt<-readOGR(paste0(dataPointLoc,"/",listDataPoint$files[listDataPoint$Code==names[i,5]]) )
   names(spPt)[2]<-"species"
   
   png(paste0(mapFolder, gsub("\\n","",names$Code[i]),"_", survey1, ".png"), height=15, width=12, units="cm", res=400)
   par(mar=c(1,1,1,1))
   plot(shoreline, col=NA, border=NA,xlim=xlim, ylim=ylim)
   plot(contours[contours@data$CONTOUR==-500,], add=T, col="black")
   plot(contours[contours@data$CONTOUR==-200,], add=T, lwd=0.7, col="black")
   plot(contours[contours@data$CONTOUR==-50,], add=T, lty=2, lwd=0.7, col="black")
   plot(ras, add=T, legend=F, col=c(paste(colors)))
   plot(nsb, add=T, col=NA, border="gray", lwd=0.8)
   plot(shoreline, col="gray", border=NA, add=T)
   plot(spPt, pch=21, cex=0.6, add=T,col=rgb(0,0,0,alpha=0.5))
   plot(spPt, pch=1, cex=0.2, add=T,col=rgb(0,0,0,alpha=0.5))
   # plot(spPt[spPt$species>0,], pch=21, cex=0.6, add=T,bg=rgb(0,0,0,alpha=0.5), col="black")
   # plot(spPt[spPt$species==0,], pch=4, cex=0.2, add=T,col=rgb(0,0,1,alpha=0.5))
 
   rng <- par("usr") # Grab the plotting region dimensions
   box( lty = 'solid', col = 'black')
   axis(side = 1, pos=rng[3], at = xSP@coords[,'xlab'], lab=xlab,  tcl = .2,padj=-2.9, cex.axis=.6)
   axis(side = 2, pos=rng[1], at = ySP@coords[,'ylab'], lab=ylab, las=1, tcl = .2,hadj=-.5, cex.axis=.6)

   # #Legend for Diversity etc
   # 
   # legend("bottomleft", legend=c("1-10th","11-20th","21-30th","31-40th","41-50th","51-60th","61-70th","71-80th","81-90th","91-100th","Sampling location"),
   #         title=c(paste0(names[i,1],"\nPercentile")), pch=c(rep(15,10),1),    col=c(colfunc(10),"black"), cex=0.7       , pt.cex=c( rep(1,10),0.4, 0.2), bty='n')
   # legend("bottom", legend=c("500 m", "200 m", "50 m", "NSB"),lty=c(6,1,2,1),lwd=c(1.2,0.7, 0.7, 0.8),col=c("black","black","black","gray"),cex=0.7, bty="n")
   # 
   #Legend for Species
   legend("bottomleft", legend=c("1-10th","11-20th","21-30th","31-40th","41-50th","51-60th","61-70th","71-80th","81-90th","91-100th"
                                 , "Species present","Species absent"
                                 ),
   title=c(paste0(names[i,3],"\nPercentile CPUE(norm)\n",names[i,9])), pch=c(15,15,15,15,15,15,15,15,15,15
                                                                             , NA,NA
                                                                             ),
   lty=c(rep(NA,10), 2,2),
   col=c(colfunc(10)
                                                                                                , "black","blue"
                                                                                                ), cex=0.7       , pt.cex=c( rep(1,10)
                                                                                                                             ,0.6,0.2
                                                                                                                             ), bty="n")
   legend("bottom", legend=c("500 m", "200 m", "50 m", "NSB"),lty=c(6,1,2,1),lwd=c(1.2,0.7, 0.7, 0.8),col=c("black","black","black","gray"),cex=0.7,
          # title=paste0(surveyName, ", ",nbhd, " km"), 
          bty="n")

   # text(850000,ylim[2]-15000,   paste(names$CommonName[i]))
   dev.off()
 }
 }
